#!/usr/bin/env python3
import argparse
import json
import os
import socket


path = os.path.dirname(os.path.abspath(__file__))

if __name__ == '__main__':
	with open(os.path.join(path, 'scanner.json')) as file:
		config = json.load(file)

	with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
		sock.bind((config['bind']['ip'], config['bind']['port']))
		sock.listen()
		while True:
			conn, addr = sock.accept()
			conn.recvfrom(2)
			data = conn.recvfrom(18)[1].decode()
			length = int(data[6:12])
			data = conn.recvfrom(length)[1].decode()
			data = dict(map(lambda s: s.split('=', 1), data.split('&')))
			if 'NAME' not in data:
				continue
			username, name = data['NAME'].split(':', 1)
			d = os.path.join(config.get('root', '/var/lib/django-printer/files/scans/'), username)
			if not os.path.isdir(d):
				if os.path.exists(d): os.remove(d)
				os.mkdir(d)
			filepath = os.path.join(d, f'{name}.pdf')
			with open(filepath, 'wb') as file:
				while True:
					data = conn.recvfrom(18)[1]
					if not data: break
					data = data.decode()
					if data.startswith('BLOCK='):
						length = int(data[6:12])
					while length > 0:
						data = conn.recvfrom(length)
						length -= len(data)
						file.write(data)
			print(f'{filepath} received')
